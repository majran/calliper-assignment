# Run code on a machine with docker and docker-compose 

1. build image (**Remember to build new image everytime you update the code**)

```bash
docker-compose build
```

2. run

```bash
docker-compose up
```

Now you can check frontend from `http://localhost/` and backend api from `http://127.0.0.1:8080/docs`


# Test codes
Run the following command on a machine with docker and docker-compose

```bash
docker-compose run fastapi ./entry.sh test
```


# Decision has been made

1. Both backend and front-end has been implemented on a same repo as it is an assignment but for real project we should have different repos
2. As in project description said "Take a minimal approach to the task, no need to use DB/authentication, error reporting, Docker, etc" I did not use database and I used mock data in files instead. That causes some part of the code (views, tests) doesn't have good style.
