"""gunicorn WSGI server configuration."""
from multiprocessing import cpu_count
from os import environ


def max_workers():
    return cpu_count()


bind = "0.0.0.0:" + environ.get("PORT", "8080")
daemon = False
max_requests = 100
timeout = 20
worker_class = "uvicorn.workers.UvicornWorker"
workers = 1  # I used 1 because I am keeping data on memory but it is better to use max_workers()
preload = True
errorlog = "-"
accesslog = "-"
loglevel = "info"
