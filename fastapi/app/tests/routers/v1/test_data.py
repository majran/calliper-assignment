from typing import Dict

from fastapi.testclient import TestClient

from app.core import settings


def test_read_data(client: TestClient) -> None:
    response = client.get(
        f'{settings.API_V1_STR}/data')
    data = response.json()
    assert response.status_code == 200
    assert len(data) > 0
