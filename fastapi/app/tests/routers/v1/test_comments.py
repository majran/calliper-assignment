import uuid

from typing import Dict

from fastapi.testclient import TestClient

from app.core import settings
from app.database.mock_comments import COMMENTS


def test_create_comment(client: TestClient, random_comment: Dict[str, str]) -> None:
    response = client.post(
        f"{settings.API_V1_STR}/comments", json=random_comment)
    comment = response.json()
    assert response.status_code == 201
    assert comment.get("text") == random_comment.get("text")
    assert comment.get("user") == random_comment.get("user")


def test_miss_required_field_create_comment(client: TestClient, random_comment: Dict[str, str]) -> None:
    random_comment.pop("datapoint")
    response = client.post(
        f"{settings.API_V1_STR}/comments", json=random_comment)
    assert response.status_code == 422


def test_invalid_datapoint_create_comment(client: TestClient, random_comment: Dict[str, str]) -> None:
    random_comment["datapoint"] = str(uuid.uuid4())
    response = client.post(
        f"{settings.API_V1_STR}/comments", json=random_comment)
    assert response.status_code == 404


def test_read_comments(client: TestClient) -> None:
    response = client.get(
        f'{settings.API_V1_STR}/comments?datapoint={str(COMMENTS[0]["datapoint"])}')
    comments = response.json()
    assert response.status_code == 200
    assert len(comments) > 0


def test_miss_required_field_read_comments(client: TestClient) -> None:
    response = client.get(
        f'{settings.API_V1_STR}/comments')
    assert response.status_code == 422
