import pytest

from typing import Dict, Generator

from fastapi.testclient import TestClient

from app.database.mock_data import DATA

from app.main import app


@pytest.fixture(scope="module")
def client() -> Generator:
    with TestClient(app) as c:
        yield c


@pytest.fixture(scope="module")
def random_comment() -> Dict[str, str]:
    return {
        "user": "user@test.com",
        "text": "This is a comment",
        "datapoint": str(DATA[0]["id"])
    }
