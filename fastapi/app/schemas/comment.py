
from pydantic import BaseModel
from uuid import UUID


class CommentBase(BaseModel):
    id: UUID
    user: str
    text: str
    datapoint: UUID


class CommentCreate(BaseModel):
    user: str
    text: str
    datapoint: UUID


class CommentResponse(CommentBase):
    ...
