
from pydantic import BaseModel
from uuid import UUID


class DataPoint(BaseModel):
    id: UUID
    value: int
    label: str
    comment_counts: int


class DataResponse(DataPoint):
    ...
