from uuid import UUID

DATA = [
    {'id': UUID('6d80a669-33aa-44d0-a044-46b39a17da18'),
     'label': 'January', 'value': 10},
    {'id': UUID('00a68254-889f-4c2f-bc6e-c4c87c50bc87'),
     'label': 'February', 'value': 14},
    {'id': UUID('324f5603-c60a-4b68-8620-3b9cd2d40369'),
     'label': 'March', 'value': 30},
    {'id': UUID('07b7b447-9313-4631-a881-6fe295a1a276'),
     'label': 'April', 'value': 18},
    {'id': UUID('d02d1919-f8e9-499d-87fb-7be50ea135c8'),
     'label': 'May', 'value': 35},
    {'id': UUID('a6d184df-99a8-4349-b546-8b05230fc8fd'),
     'label': 'June', 'value': 23},
    {'id': UUID('c277f58d-5e6f-49a3-a512-8b292f27c716'),
     'label': 'July', 'value': 40}
]
