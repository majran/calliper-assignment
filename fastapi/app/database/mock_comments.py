from uuid import UUID

COMMENTS = [
    {'id': UUID('98ab5b25-b256-4c63-8e5d-8cb2995f2869'),
     'user': 'majid@test.com',
             'datapoint': UUID('324f5603-c60a-4b68-8620-3b9cd2d40369'),
             'text': 'This is awsome!'},
    {'id': UUID('024cf0a9-e87f-4334-a0c9-4e459030f41d'),
     'user': 'majid@test.com',
             'datapoint': UUID('324f5603-c60a-4b68-8620-3b9cd2d40369'),
             'text': 'Why this happend?'},
    {'id': UUID('4a875692-56ee-4304-b64e-c62fb5fc79cb'),
     'user': 'majid@test.com',
             'datapoint': UUID('c277f58d-5e6f-49a3-a512-8b292f27c716'),
             'text': 'Sounds we have an issue'}
]
