import copy

from typing import Any, List

from fastapi import APIRouter

from app import schemas
from app.database.mock_data import DATA
from app.database.mock_comments import COMMENTS

router = APIRouter()


@router.get("", response_model=List[schemas.DataResponse])
def get() -> Any:
    """
    Retrieve all data.
    """

    # add comments_count for each datapoint
    comment_counts = {}
    for comment in COMMENTS:
        comment_counts[comment["datapoint"]] = comment_counts.get(
            comment["datapoint"], 0) + 1

    result = copy.deepcopy(DATA)
    for datapoint in result:
        datapoint["comment_counts"] = comment_counts.get(
            datapoint["id"], 0)
    return result
