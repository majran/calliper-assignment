
from fastapi import APIRouter

from app.routers.v1 import comments
from app.routers.v1 import data

api_router = APIRouter()
api_router.include_router(
    comments.router, prefix="/comments", tags=["comments"])

api_router.include_router(
    data.router, prefix="/data", tags=["data"])
