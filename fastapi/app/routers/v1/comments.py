import uuid

from typing import Any, List

from fastapi import APIRouter, status, HTTPException

from app import schemas
from app.database.mock_comments import COMMENTS
from app.database.mock_data import DATA

router = APIRouter()


@router.get("", response_model=List[schemas.CommentResponse])
def list(datapoint: uuid.UUID, skip: int = 0, limit: int = 10) -> Any:
    """
    Retrieve all comments for a datapoint.
    """
    comments = [
        comment for comment in COMMENTS if comment["datapoint"] == datapoint]
    return comments


@router.post("", response_model=schemas.CommentResponse, status_code=status.HTTP_201_CREATED)
def create(request: schemas.CommentCreate) -> Any:
    """
    Create new comment.
    """
    datapoints_id = [datapoint["id"] for datapoint in DATA]
    if request.datapoint not in datapoints_id:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="The datapoint with this ID does not exist in the system.",
        )
    comment = request.dict()
    comment.update({'id': uuid.uuid4()})

    COMMENTS.append(comment)

    return comment
