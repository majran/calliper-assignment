#!/bin/sh

set -e

orig=$@
cmd=$1
shift

if [ $cmd = 'web' ]; then
  exec gunicorn -c gunicorn.py app.main:app --reload
elif [ $cmd = 'test' ]; then
  exec pytest
fi

exec "$orig"