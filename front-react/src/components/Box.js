import React from "react";
import Box from "@mui/material/Box";

export default function BoxComponent({ children, bgcolor, ...props }) {
  return (
    <Box
      style={{
        backgroundColor: bgcolor,
      }}
      {...props}
    >
      {children}
    </Box>
  );
}
