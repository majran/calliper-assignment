import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

import { setStatus, getStatus } from "store/commonSlice";

export default function CustomSnackBar() {
  const dispatch = useDispatch();
  const status = useSelector(getStatus);
  const [severity, setSeverity] = useState("success");
  const [msg, setMsg] = useState("All is well.");

  // this makes sure that Alert will always have data and it won't be an empty.
  useEffect(() => {
    if (status.type) {
      setSeverity(status.type);
      setMsg(status.msg);
    }
  }, [status.type, status.msg]);

  return (
    <Snackbar
      open={status.type !== ""}
      autoHideDuration={6000}
      onClose={() => dispatch(setStatus())}
    >
      <Alert severity={severity}>{msg}</Alert>
    </Snackbar>
  );
}
