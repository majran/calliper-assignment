import React from "react";

import Box from "components/Box";

export default function CenteringBox({ children, ...props }) {
  return (
    <Box alignItems="center" display="flex" justifyContent="center" {...props}>
      {children}
    </Box>
  );
}
