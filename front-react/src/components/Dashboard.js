import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Chart from "chart.js/auto";
import { Line } from "react-chartjs-2";
import ChartDataLabels from "chartjs-plugin-datalabels";

import CenteringBox from "components/CenteringBox";
import { getData } from "store/commonSlice";
import CommentDialog from "pages/Comment/components/CommentDialog";
import { setSelectedDataPoint } from "pages/Comment/commentSlice";

const Graph = (props) => {
  const [openComments, setOpenComments] = useState(false);
  const dispatch = useDispatch();

  Chart.register(ChartDataLabels);

  const fetch_data = useSelector(getData);

  let labels = [];
  let commentCounts = [];
  let dataPoint = [];

  fetch_data.forEach((item) => {
    labels.push(item.label);
    dataPoint.push(item.value);
    commentCounts.push(item.comment_counts);
  });

  const data = {
    labels,
    datasets: [
      {
        label: "Monthly active user",
        data: dataPoint,
        fill: false,
        backgroundColor: "transparent",
        borderColor: "#06a1e1",
        tension: 0.1,
        borderWidth: 4,
      },
      {
        data: commentCounts,
        hidden: true,
      },
    ],
  };

  const options = {
    responsive: true,
    maintainAspectRatio: true,
    onClick: (e, elements) => {
      let dataPointIndex = elements[0]?.index;
      if (typeof dataPointIndex == "number") {
        dispatch(setSelectedDataPoint(fetch_data[dataPointIndex].id));
        setOpenComments(true);
      }
    },
    scales: {
      x: {
        grid: {
          display: false,
        },
      },
      y: {
        grid: {
          display: false,
        },
      },
    },
    plugins: {
      legend: {
        display: false,
      },
      title: {
        display: true,
        text: "Monthly active user",
        padding: {
          bottom: 30,
        },
        weight: "bold",
        color: "#00325c",
        font: {
          size: 13,
        },
        align: "start",
      },
      datalabels: {
        display: true,
        color: "black",
        align: "end",
        padding: {
          right: 2,
        },
        labels: {
          padding: { top: 10 },
          title: {
            font: {
              weight: "bold",
            },
          },
          value: {
            color: "green",
          },
        },
        formatter: function (value, context) {
          let counts = context.chart.data.datasets[1].data[context.dataIndex];
          return counts > 0 ? counts : null;
        },
      },
    },
  };

  return (
    <CenteringBox>
      <CommentDialog open={openComments} setOpen={setOpenComments} />
      <Line data={data} options={options} />
    </CenteringBox>
  );
};
export default Graph;
