import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { ThemeProvider, createTheme } from "@mui/material/styles";
import CircularProgress from "@mui/material/CircularProgress";

import CenteringBox from "components/CenteringBox";
import CustomSnackBar from "components/CustomSnackBar";
import Dashboard from "components/Dashboard";
import { getDataRequestAsync, getAppLoaded } from "store/commonSlice";

const mdTheme = createTheme();

function App() {
  const appLoaded = useSelector(getAppLoaded);
  const dispatch = useDispatch();

  useEffect(() => {
    // Using an IIFE to avoid returning a promise from useEffect
    (async function intializeApp() {
      await dispatch(getDataRequestAsync());
    })();
  }, [dispatch]);
  return (
    <ThemeProvider theme={mdTheme}>
      {!appLoaded ? (
        <CenteringBox height="100vh">
          <CircularProgress />
        </CenteringBox>
      ) : (
        <Dashboard>
          <CustomSnackBar />
        </Dashboard>
      )}
    </ThemeProvider>
  );
}

export default App;
