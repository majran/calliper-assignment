import { configureStore } from "@reduxjs/toolkit";

// import all specific reducers here

import commentReducer from "pages/Comment/commentSlice";

import commonReducer from "store/commonSlice";

const reducer = {
  common: commonReducer,
  // OTHER REDUCERS will be added here

  comment: commentReducer,
};

// mostly for testing purposes you can set a preloaded state here
const preloadedState = {
  // preloadedState: true
};

// this middleware manages jwt token for us
const localStorageMiddleware = (store) => (next) => (action) => {
  console.log(action.type);
  next(action);
};

export default configureStore({
  reducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(localStorageMiddleware),
  devTools: process.env.NODE_ENV !== "production",
  preloadedState,
});
