import { createSlice } from "@reduxjs/toolkit";

import api from "api";

export const commonSlice = createSlice({
  name: "common",
  initialState: {
    appLoaded: false,
    data: null,
    status: {
      type: "",
      msg: "",
    },
  },
  reducers: {
    setAppLoaded: (state, action) => {
      state.appLoaded = action.payload;
    },
    setData: (state, action) => {
      state.data = action.payload;
    },
    setStatus: (state, action) => {
      if (!action.payload) {
        state.status = {
          type: "",
          msg: "",
        };
      } else {
        state.status = action.payload;
      }
    },
  },
});

export const { setAppLoaded, setStatus, setData } = commonSlice.actions;

export const getData = (state) => state.common.data;
export const getAppLoaded = (state) => state.common.appLoaded;
export const getStatus = (state) => state.common.status;

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched
export const getDataRequestAsync = () => async (dispatch) => {
  try {
    const data = await api.Data.getDataList();
    dispatch(setData(data));
    setTimeout(function () {
      dispatch(setAppLoaded(true));
    }, 1000);
  } catch (e) {
    console.log(e);
    dispatch(
      setStatus({
        type: "error",
        msg: e.message,
      })
    );
  }
};

export const raiseError = (e) => async (dispatch) => {
  dispatch(
    setStatus({
      type: "error",
      msg: e.message,
    })
  );
};

export default commonSlice.reducer;
