import { createSlice } from "@reduxjs/toolkit";

import api from "api";

import { raiseError, getDataRequestAsync } from "store/commonSlice";

export const commentSlice = createSlice({
  name: "comment",
  initialState: {
    commentList: [],
    isFetching: false,
    selectedDataPoint: null,
  },
  reducers: {
    apiRequest: (state) => {
      state.isFetching = true;
    },
    commentListSuccess: (state, action) => {
      state.commentList = action.payload;
      state.isFetching = false;
    },
    setSelectedDataPoint: (state, action) => {
      state.selectedDataPoint = action.payload;
    },
    apiFailure: (state) => {
      state.isFetching = false;
    },
  },
});

export const {
  apiRequest,
  apiFailure,
  commentListSuccess,
  setSelectedDataPoint,
} = commentSlice.actions;

export const commentListRequestAsync = (dataPoint) => async (dispatch) => {
  dispatch(apiRequest());
  try {
    const res = await api.Comment.getCommentList(dataPoint);
    dispatch(commentListSuccess(res));
    dispatch(getDataRequestAsync());
  } catch (e) {
    console.log(e);
    dispatch(raiseError(e));
    dispatch(apiFailure());
  }
};

export const addCommentRequestAsync =
  ({ data, setSubmitting, setOpen }) =>
  async (dispatch) => {
    dispatch(apiRequest());
    try {
      await api.Comment.addComment(data);
      setSubmitting(false);
      setOpen(false);
      dispatch(commentListRequestAsync(data.datapoint));
    } catch (e) {
      console.log(e);
      dispatch(raiseError(e));
      dispatch(apiFailure());
    }
  };

export const getCommentList = (state) => state.comment.commentList;
export const getSelectedDataPoint = (state) => state.comment.selectedDataPoint;

export default commentSlice.reducer;
