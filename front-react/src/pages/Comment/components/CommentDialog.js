import * as React from "react";
import { useSelector, useDispatch } from "react-redux";
import Avatar from "@mui/material/Avatar";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import ListItemText from "@mui/material/ListItemText";
import DialogTitle from "@mui/material/DialogTitle";
import Dialog from "@mui/material/Dialog";
import PersonIcon from "@mui/icons-material/Person";
import AddIcon from "@mui/icons-material/Add";
import { blue } from "@mui/material/colors";

import {
  getCommentList,
  commentListRequestAsync,
  getSelectedDataPoint,
} from "pages/Comment/commentSlice";
import CommentForm from "pages/Comment/components/CommentForm";

export default function SimpleDialog({ open, setOpen }) {
  const [openAddComments, setOpenAddComments] = React.useState(false);
  const selectedDataPoint = useSelector(getSelectedDataPoint);

  const dispatch = useDispatch();

  React.useEffect(() => {
    if (selectedDataPoint) {
      dispatch(commentListRequestAsync(selectedDataPoint));
    }
  }, [dispatch, selectedDataPoint]);

  const comments = useSelector(getCommentList);
  return (
    <Dialog onClose={() => setOpen(false)} open={open}>
      <CommentForm
        open={openAddComments}
        setOpen={setOpenAddComments}
      ></CommentForm>
      <DialogTitle>Comments</DialogTitle>
      <List sx={{ pt: 0 }}>
        {comments.map(({ id, user, text }) => (
          <ListItem key={id}>
            <ListItemAvatar>
              <Avatar sx={{ bgcolor: blue[100], color: blue[600] }}>
                <PersonIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={user} secondary={text} />
          </ListItem>
        ))}
        <ListItem autoFocus button onClick={() => setOpenAddComments(true)}>
          <ListItemAvatar>
            <Avatar>
              <AddIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary="Add comment" />
        </ListItem>
      </List>
    </Dialog>
  );
}
