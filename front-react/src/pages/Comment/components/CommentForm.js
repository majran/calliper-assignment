import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";

import { Grid } from "@mui/material";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import CloseIcon from "@mui/icons-material/Close";
import Slide from "@mui/material/Slide";
import TextField from "@mui/material/TextField";

import {
  addCommentRequestAsync,
  getSelectedDataPoint,
} from "pages/Comment/commentSlice";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function CommentForm({ open, setOpen }) {
  const dispatch = useDispatch();
  const ValidationSchema = Yup.object().shape({
    user: Yup.string().required("Required"),
    text: Yup.string().required("Required"),
  });

  const selectedDataPoint = useSelector(getSelectedDataPoint);

  const initialValues = () => {
    var result = {
      user: "",
      text: "",
    };
    return result;
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Formik
      initialValues={initialValues()}
      validationSchema={ValidationSchema}
      onSubmit={async (values, { setSubmitting, resetForm }) => {
        // clear null values
        Object.keys(values).forEach(
          (key) => values[key] == null && delete values[key]
        );
        setSubmitting(true);
        dispatch(
          addCommentRequestAsync({
            data: { ...values, datapoint: selectedDataPoint },
            setSubmitting,
            setOpen,
          })
        );
      }}
    >
      {({ values, errors, handleChange, handleSubmit, isSubmitting }) => (
        <Dialog
          fullScreen
          open={open}
          onClose={handleClose}
          TransitionComponent={Transition}
        >
          <form onSubmit={handleSubmit}>
            <AppBar style={{ position: "relative" }}>
              <Toolbar>
                <IconButton
                  edge="start"
                  color="inherit"
                  onClick={handleClose}
                  aria-label="close"
                >
                  <CloseIcon />
                </IconButton>
                <Typography variant="h6" style={{ flex: 1 }}>
                  New comment
                </Typography>
                <Button color="inherit" type="submit" disabled={isSubmitting}>
                  save
                </Button>
              </Toolbar>
            </AppBar>
            <Grid
              item
              container
              xs={12}
              sm={5}
              spacing={2}
              direction="column"
              alignContent="center"
              justifyContent="space-between"
            >
              <Grid item justify="space-around" xs={12} sm={5}>
                <TextField
                  autoFocus
                  margin="dense"
                  onChange={handleChange}
                  value={values.user}
                  inputProps={{
                    name: "user",
                    id: "user",
                  }}
                  label="User Email"
                  type="email"
                  error={errors.user ? true : false}
                  helperText={errors.user}
                />
              </Grid>
              <Grid item>
                <TextField
                  style={{ minWidth: "25rem" }}
                  label="Text"
                  placeholder="Add comment"
                  multiline
                  rows={5}
                  variant="outlined"
                  onChange={handleChange}
                  value={values.text}
                  error={errors.text ? true : false}
                  helperText={errors.text}
                  inputProps={{
                    name: "text",
                    id: "text",
                  }}
                />
              </Grid>
            </Grid>
          </form>
        </Dialog>
      )}
    </Formik>
  );
}
