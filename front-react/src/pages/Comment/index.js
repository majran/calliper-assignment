import React from "react";

import { Grid } from "@mui/material";

import CommentForm from "./components/CommentForm";
import CommentTabel from "./components/CommentTabel";

export default function Comment() {
  return (
    <Grid
      container
      direction="column"
      alignContent="center"
      justifyContent="center"
      spacing={5}
    >
      <Grid item container xs={12} sm={11}>
        <CommentForm />
      </Grid>
      <Grid item container xs={12} sm={11}>
        <CommentTabel />
      </Grid>
    </Grid>
  );
}
