import { requests, objectResponseBody } from "api";

const Comment = {
  getCommentList: (dataPoint) =>
    requests.get(`comments/?datapoint=${dataPoint}`),
  addComment: (data) =>
    requests.post(`comments/`, data).then(objectResponseBody),
};

export default Comment;
