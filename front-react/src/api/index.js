import superagentPromise from "superagent-promise";
import _superagent from "superagent";

import Data from "./Data";
import Comment from "./Comment";

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = process.env.REACT_APP_API_ROOT;

const responseBody = (res) => res.body;

export const listResponseBody = (res) => res.data.results;
export const objectResponseBody = (res) => res.data;

export const requests = {
  get: (url) => superagent.get(`${API_ROOT}${url}`).then(responseBody),
  post: (url, body) =>
    superagent.post(`${API_ROOT}${url}`, body).then(responseBody),
};

export const corsRequests = {
  get: (url) => superagent.get(`${url}`).then(responseBody),
};

const apis = {
  Data,
  Comment,
};

export default apis;
