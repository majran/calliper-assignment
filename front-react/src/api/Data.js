import { requests } from "api";

const Data = {
  getDataList: () => requests.get(`data/`),
};

export default Data;
